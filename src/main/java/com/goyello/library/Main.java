package com.goyello.library;

import com.goyello.library.flow.MainFlows;
import com.goyello.library.service.FileService;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        if (args != null && args.length > 0) {
            System.out.println("Path to file set to: " + args[0]);
            FileService.setFilePath(args[0]);
        } else {
            System.out.println("Using default file path: " + FileService.getFilePath());
        }


        System.out.println("Started Main application");
        boolean isRunning = true;
        Scanner scanner;
        while (isRunning) {
            printFunctionsList();
            scanner = new Scanner(System.in);
            int functionNumber = scanner.nextInt();
            performActionBasedOnNumber(functionNumber);

        }
    }

    public static void printFunctionsList() {
        System.out.println("What do you want to do?");
        System.out.println("1. Add book");
        System.out.println("2. Delete book by isbn");
        System.out.println("3. Find book by title");
        System.out.println("4. Find book by author");
        System.out.println("5. Find book by isbn");
        System.out.println("6. Find books not borrow for last x weeks");
        System.out.println("7. Borrow book");
        System.out.println("8. Show list of customers with borrowed books number");
        System.out.println("9. Exit");
    }

    public static void performActionBasedOnNumber(int number) {
        MainFlows mainFlows = new MainFlows();
        switch (number) {
            case 1:
                mainFlows.addBookFlow();
                break;
            case 2:
                mainFlows.deleteBookFlow();
                break;
            case 3:
                mainFlows.findBookByTitleFlow();
                break;
            case 4:
                mainFlows.findBookByAuthorFlow();
                break;
            case 5:
                mainFlows.findBookByIsbnFlow();
                break;
            case 6:
                mainFlows.findBookByLastBorrowDateLessThanNumberOfWeeksFlow();
                break;
            case 7:
                mainFlows.borrowBookFlow();
                break;
            case 8:
                mainFlows.customerListFlow();
                break;
            case 9:
                System.exit(0);
            default:
                System.out.println("Wrong command number: " + number);
        }
    }


}
