package com.goyello.library.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@ToString
public class Book {

    private String name;
    private String author;
    private long isbn;

    public Book(String name, String author, long isbn) {
        this.name = name;
        this.author = author;
        this.isbn = isbn;
    }

    @Setter
    private LocalDateTime lastBorrowDate;
    @Setter
    private Customer customer;

}
