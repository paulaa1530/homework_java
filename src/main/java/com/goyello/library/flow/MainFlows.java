package com.goyello.library.flow;

import com.goyello.library.domain.Book;
import com.goyello.library.domain.Customer;
import com.goyello.library.service.BookService;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class MainFlows {
    private static final BookService bookService = new BookService();


    public void addBookFlow() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type book title");
        String title = scanner.nextLine();
        System.out.println("Type book author");
        String author = scanner.nextLine();
        System.out.println("Type book isbn");
        long isbn = scanner.nextLong();
        bookService.addBook(title, author, isbn);
    }


    public void deleteBookFlow() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type book isbn that has to be deleted");
        long isbn = scanner.nextLong();
        bookService.deleteBookByISBN(isbn);
    }


    public void findBookByTitleFlow() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type book title");
        String title = scanner.nextLine();
        List<Book> bookList = bookService.findBookByTitle(title);

        System.out.println("Founded books: ");
        bookList.stream().forEach(System.out::println);
    }

    public void findBookByAuthorFlow() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type book author");
        String author = scanner.nextLine();
        List<Book> bookList = bookService.findBookByAuthor(author);

        System.out.println("Founded books: ");
        bookList.stream().forEach(System.out::println);
    }

    public void findBookByIsbnFlow() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type book isbn");
        long isbn = scanner.nextLong();
        Optional<Book> bookOptional = bookService.findBookByISBN(isbn);

        if (bookOptional.isPresent()) {
            System.out.println("Founded books: " + bookOptional.get());
        } else {
            System.out.println("There is no book with that isbn");
        }
    }

    public void findBookByLastBorrowDateLessThanNumberOfWeeksFlow() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type number of weeks");
        long numberOfWeeks = scanner.nextLong();
        List<Book> bookList = bookService.findBookByLastBorrowDateLessThanNumberOfWeeks(numberOfWeeks);

        if (!bookList.isEmpty()) {
            System.out.println("Founded books: ");
            bookList.stream().forEach(System.out::println);
        } else {
            System.out.println("There is no books for given time range");
        }
    }

    public void borrowBookFlow() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type isbn of book that you want to borrow");
        long isbn = scanner.nextLong();

        scanner = new Scanner(System.in);
        System.out.println("Type your name");
        String customerName = scanner.nextLine();

        scanner = new Scanner(System.in);
        System.out.println("Type your surname");
        String customerSurname = scanner.nextLine();

        bookService.borrowBook(isbn, customerName, customerSurname);
    }

    public void customerListFlow() {
        List<Customer> customerList = bookService.getCustomerList();

        if (!customerList.isEmpty()) {
            System.out.println("Customer list: ");
            customerList.stream().forEach(System.out::println);
        } else {
            System.out.println("No customer found");
        }
    }
}
