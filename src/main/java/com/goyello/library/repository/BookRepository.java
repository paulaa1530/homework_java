package com.goyello.library.repository;

import com.goyello.library.domain.Book;
import com.goyello.library.domain.Customer;
import com.goyello.library.service.FileService;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class BookRepository {

    private static List<Book> bookList;

    private final FileService fileService = new FileService();

    private static final BookRepository bookRepository = new BookRepository();

    private BookRepository() {
        if (bookList == null) {
            bookList = fileService.readDataFromFile();
        }
    }

    public static BookRepository getInstance() {
        return bookRepository;
    }

    public boolean addBook(Book book) {
        boolean result = bookList.add(book);
        updateFile();
        return result;
    }

    public List<Book> findBookByTitle(String title) {
        return bookList.stream()
                .filter(book -> book.getName().equalsIgnoreCase(title))
                .collect(Collectors.toList());
    }

    public Optional<Book> findBookByISBN(long isbn) {
        return bookList.stream()
                .filter(book -> book.getIsbn() == isbn)
                .findFirst();
    }

    public List<Book> findBookByAuthor(String author) {
        return bookList.stream()
                .filter(book -> book.getAuthor().equalsIgnoreCase(author))
                .collect(Collectors.toList());
    }

    public boolean deleteBook(Book book) {
        boolean result = bookList.remove(book);
        updateFile();
        return result;
    }

    public List<Book> findBookByLastBorrowDateLessThanNumberOfWeeks(long numberOfWeeks) {
        return bookList.stream()
                .filter(book -> Objects.nonNull(book.getLastBorrowDate()))
                .filter(book -> book.getLastBorrowDate().compareTo(LocalDateTime.now().minusWeeks(numberOfWeeks)) < 0)
                .collect(Collectors.toList());
    }

    public void borrowBook(long isbn, Customer customer) {
        Optional<Book> bookOptional = findBookByISBN(isbn);

        if (bookOptional.isPresent()) {
            Book book = bookOptional.get();
            book.setCustomer(customer);
            book.setLastBorrowDate(LocalDateTime.now());
            updateFile();
            System.out.println("Book borrowed successfully");
        } else {
            System.out.println("Book with isbn: " + isbn + " not found");
        }
    }

    public List<Customer> getCustomerList() {
        Map<Customer, List<Book>> customerBookListMap = bookList.stream()
                .filter(book -> Objects.nonNull(book.getCustomer()))
                .collect(Collectors.groupingBy(Book::getCustomer));

        return customerBookListMap.entrySet().stream()
                .map(customerListEntry -> {
                    Customer customer = customerListEntry.getKey();
                    customer.setNumberOfBorrowedBooks(customerListEntry.getValue().size());
                    return customer;
                }).collect(Collectors.toList());

    }

    private void updateFile() {
        fileService.writeDataToFile(bookList);
    }
}
