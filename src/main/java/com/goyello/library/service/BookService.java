package com.goyello.library.service;

import com.goyello.library.domain.Book;
import com.goyello.library.domain.Customer;
import com.goyello.library.repository.BookRepository;
import com.goyello.library.validator.BookValidator;
import com.goyello.library.validator.CustomerValidator;

import java.util.List;
import java.util.Optional;

public class BookService {

    private static final BookRepository bookRepository = BookRepository.getInstance();

    public void addBook(String title, String author, long isbn) {
        Book book = new Book(title, author, isbn);
        if (BookValidator.validate(book)) {
            Optional<Book> bookOptional = bookRepository.findBookByISBN(isbn);

            if (bookOptional.isPresent()) {
                System.out.println("Book with this isbn already exists!!!");
            } else {
                if (bookRepository.addBook(book)) {
                    System.out.println("Book added successfully");
                }
                ;
            }
        }
    }

    public void deleteBookByISBN(long isbn) {
        Optional<Book> bookOptional = bookRepository.findBookByISBN(isbn);
        if (bookOptional.isPresent()) {
            if (bookRepository.deleteBook(bookOptional.get())) {
                System.out.println("Book deleted successfully");
            }
        } else {
            System.out.println("There is no book with following isbn: " + isbn);
        }
    }

    public void borrowBook(long isbn, String customerName, String customerSurname) {
        Customer customer = new Customer(customerName, customerSurname);
        if (CustomerValidator.validate(customer)) {
            bookRepository.borrowBook(isbn, customer);
        } else {
            System.out.println("Not valid customer data");
        }
    }

    public List<Book> findBookByTitle(String title) {
        return bookRepository.findBookByTitle(title);
    }

    public Optional<Book> findBookByISBN(long isbn) {
        return findBookByISBN(isbn);
    }

    public List<Book> findBookByAuthor(String author) {
        return bookRepository.findBookByAuthor(author);
    }

    public List<Book> findBookByLastBorrowDateLessThanNumberOfWeeks(long numberOfWeeks) {
        return bookRepository.findBookByLastBorrowDateLessThanNumberOfWeeks(numberOfWeeks);
    }

    public List<Customer> getCustomerList() {
        return bookRepository.getCustomerList();
    }

}
