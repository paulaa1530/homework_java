package com.goyello.library.service;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.goyello.library.domain.Book;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FileService {

    private static String filePath = System.getProperty("user.dir") + "/booksFile.txt";

    public List<Book> readDataFromFile() {
        try (FileReader fileReader = new FileReader(filePath);
             JsonReader jsonReader = new JsonReader(fileReader)) {
            List<Book> bookList = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonReader, Book[].class)));
            System.out.println("Loaded books from file with number: " + bookList.size());
            return bookList;
        } catch (FileNotFoundException e) {
            return Collections.emptyList();
        } catch (IOException e) {
            System.out.println("Application couldn't read books file");
            System.out.println("Cause: " + e.getMessage());
        }

        return Collections.emptyList();
    }

    public void writeDataToFile(List<Book> bookList) {
        String jsonString = new Gson().toJson(bookList);
        try (FileWriter file = new FileWriter(filePath)) {
            file.write(jsonString);
            System.out.println("Successfully Copied JSON Object to file");
        } catch (IOException e) {
            System.out.println("Saving books unsuccessfully");
            System.out.println("Cause: " + e.getMessage());
        }
    }


    public static void setFilePath(String path) {
        filePath = path;
    }

    public static String getFilePath() {
        return filePath;
    }
}
