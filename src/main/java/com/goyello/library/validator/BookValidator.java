package com.goyello.library.validator;

import com.goyello.library.domain.Book;

public class BookValidator {

    public static boolean validate(Book book) {
        if (String.valueOf(book.getIsbn()).length() != 13) {
            System.out.println("Isbn should have 13 digits");
            return false;
        }
        if (book.getName() == null || book.getName().isEmpty()) {
            System.out.println("Book name is required");
            return false;
        }
        if (book.getAuthor() == null || book.getAuthor().isEmpty()) {
            System.out.println("Book author is required");
            return false;
        }

        return true;
    }
}
