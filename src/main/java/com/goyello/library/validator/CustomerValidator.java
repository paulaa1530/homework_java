package com.goyello.library.validator;

import com.goyello.library.domain.Customer;

public class CustomerValidator {

    public static boolean validate(Customer customer) {
        return hasName(customer.getName())
                && hasSurname(customer.getSurname());
    }

    private static boolean hasName(String name) {
        return name != null && !name.isEmpty();
    }

    private static boolean hasSurname(String surname) {
        return surname != null && !surname.isEmpty();
    }
}
